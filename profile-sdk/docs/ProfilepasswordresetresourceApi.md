# ProfilepasswordresetresourceApi

All URIs are relative to *https://localhost:8082/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**isVaildTokenUsingGET**](ProfilepasswordresetresourceApi.md#isVaildTokenUsingGET) | **GET** /api/token/isVaildPasswordToken | isVaildToken
[**resendVerificationTokenUsingPOST**](ProfilepasswordresetresourceApi.md#resendVerificationTokenUsingPOST) | **POST** /api/token/resendVerificationToken | resendVerificationToken
[**sendResetPasswordTokenUsingGET**](ProfilepasswordresetresourceApi.md#sendResetPasswordTokenUsingGET) | **GET** /api/token/sendResetPasswordToken | Send Reset password token
[**updateVerificationTokenUsingPOST**](ProfilepasswordresetresourceApi.md#updateVerificationTokenUsingPOST) | **POST** /api/token/updateVerificationToken | updateVerificationToken


<a name="isVaildTokenUsingGET"></a>
# **isVaildTokenUsingGET**
> PasswordTokenResponse isVaildTokenUsingGET(token)

isVaildToken

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfilepasswordresetresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfilepasswordresetresourceApi apiInstance = new ProfilepasswordresetresourceApi();
String token = "token_example"; // String | token
try {
    PasswordTokenResponse result = apiInstance.isVaildTokenUsingGET(token);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilepasswordresetresourceApi#isVaildTokenUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **String**| token |

### Return type

[**PasswordTokenResponse**](PasswordTokenResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="resendVerificationTokenUsingPOST"></a>
# **resendVerificationTokenUsingPOST**
> EmailResponse resendVerificationTokenUsingPOST(request)

resendVerificationToken

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfilepasswordresetresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfilepasswordresetresourceApi apiInstance = new ProfilepasswordresetresourceApi();
TokenRequest request = new TokenRequest(); // TokenRequest | request
try {
    EmailResponse result = apiInstance.resendVerificationTokenUsingPOST(request);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilepasswordresetresourceApi#resendVerificationTokenUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**TokenRequest**](TokenRequest.md)| request |

### Return type

[**EmailResponse**](EmailResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="sendResetPasswordTokenUsingGET"></a>
# **sendResetPasswordTokenUsingGET**
> TokenStatusResponse sendResetPasswordTokenUsingGET(email)

Send Reset password token

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfilepasswordresetresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfilepasswordresetresourceApi apiInstance = new ProfilepasswordresetresourceApi();
String email = "email_example"; // String | email
try {
    TokenStatusResponse result = apiInstance.sendResetPasswordTokenUsingGET(email);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilepasswordresetresourceApi#sendResetPasswordTokenUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| email |

### Return type

[**TokenStatusResponse**](TokenStatusResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="updateVerificationTokenUsingPOST"></a>
# **updateVerificationTokenUsingPOST**
> EmailResponse updateVerificationTokenUsingPOST(request)

updateVerificationToken

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfilepasswordresetresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfilepasswordresetresourceApi apiInstance = new ProfilepasswordresetresourceApi();
TokenRequest request = new TokenRequest(); // TokenRequest | request
try {
    EmailResponse result = apiInstance.updateVerificationTokenUsingPOST(request);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfilepasswordresetresourceApi#updateVerificationTokenUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**TokenRequest**](TokenRequest.md)| request |

### Return type

[**EmailResponse**](EmailResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

