# PersonaresourceApi

All URIs are relative to *https://localhost:8082/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**changePersonaToDefaultUsingPOST**](PersonaresourceApi.md#changePersonaToDefaultUsingPOST) | **POST** /api/persona/changePersonaToDefault | changePersonaToDefault
[**createPersonaWithPreferencesUsingPOST**](PersonaresourceApi.md#createPersonaWithPreferencesUsingPOST) | **POST** /api/persona/createPersonaWithPreferences | createPersonaWithPreferences
[**createPersonaWithoutPreferencesUsingPOST**](PersonaresourceApi.md#createPersonaWithoutPreferencesUsingPOST) | **POST** /api/persona/createPersonaWithoutPreferences | createPersonaWithoutPreferences
[**getAllPersonasUsingGET**](PersonaresourceApi.md#getAllPersonasUsingGET) | **GET** /api/persona/getAllPersonas | getAllPersonas
[**getDefaultPersonaUsingGET**](PersonaresourceApi.md#getDefaultPersonaUsingGET) | **GET** /api/persona/getDefaultPersona | getDefaultPersona
[**updatePersonaStatusUsingPOST**](PersonaresourceApi.md#updatePersonaStatusUsingPOST) | **POST** /api/persona/updateDefaultPersona | updatePersonaStatus


<a name="changePersonaToDefaultUsingPOST"></a>
# **changePersonaToDefaultUsingPOST**
> PersonaResponse changePersonaToDefaultUsingPOST(request)

changePersonaToDefault

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PersonaresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

PersonaresourceApi apiInstance = new PersonaresourceApi();
PersonaRequest request = new PersonaRequest(); // PersonaRequest | request
try {
    PersonaResponse result = apiInstance.changePersonaToDefaultUsingPOST(request);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PersonaresourceApi#changePersonaToDefaultUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**PersonaRequest**](PersonaRequest.md)| request |

### Return type

[**PersonaResponse**](PersonaResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="createPersonaWithPreferencesUsingPOST"></a>
# **createPersonaWithPreferencesUsingPOST**
> PersonaResponse createPersonaWithPreferencesUsingPOST(request)

createPersonaWithPreferences

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PersonaresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

PersonaresourceApi apiInstance = new PersonaresourceApi();
PersonaRequest request = new PersonaRequest(); // PersonaRequest | request
try {
    PersonaResponse result = apiInstance.createPersonaWithPreferencesUsingPOST(request);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PersonaresourceApi#createPersonaWithPreferencesUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**PersonaRequest**](PersonaRequest.md)| request |

### Return type

[**PersonaResponse**](PersonaResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="createPersonaWithoutPreferencesUsingPOST"></a>
# **createPersonaWithoutPreferencesUsingPOST**
> PersonaResponse createPersonaWithoutPreferencesUsingPOST(request)

createPersonaWithoutPreferences

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PersonaresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

PersonaresourceApi apiInstance = new PersonaresourceApi();
PersonaRequest request = new PersonaRequest(); // PersonaRequest | request
try {
    PersonaResponse result = apiInstance.createPersonaWithoutPreferencesUsingPOST(request);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PersonaresourceApi#createPersonaWithoutPreferencesUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**PersonaRequest**](PersonaRequest.md)| request |

### Return type

[**PersonaResponse**](PersonaResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getAllPersonasUsingGET"></a>
# **getAllPersonasUsingGET**
> PersonaRequestWrapper getAllPersonasUsingGET(email)

getAllPersonas

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PersonaresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

PersonaresourceApi apiInstance = new PersonaresourceApi();
String email = "email_example"; // String | email
try {
    PersonaRequestWrapper result = apiInstance.getAllPersonasUsingGET(email);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PersonaresourceApi#getAllPersonasUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| email |

### Return type

[**PersonaRequestWrapper**](PersonaRequestWrapper.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getDefaultPersonaUsingGET"></a>
# **getDefaultPersonaUsingGET**
> PersonaResponse getDefaultPersonaUsingGET(email)

getDefaultPersona

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PersonaresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

PersonaresourceApi apiInstance = new PersonaresourceApi();
String email = "email_example"; // String | email
try {
    PersonaResponse result = apiInstance.getDefaultPersonaUsingGET(email);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PersonaresourceApi#getDefaultPersonaUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| email |

### Return type

[**PersonaResponse**](PersonaResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="updatePersonaStatusUsingPOST"></a>
# **updatePersonaStatusUsingPOST**
> Integer updatePersonaStatusUsingPOST(request)

updatePersonaStatus

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PersonaresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

PersonaresourceApi apiInstance = new PersonaresourceApi();
PersonaRequest request = new PersonaRequest(); // PersonaRequest | request
try {
    Integer result = apiInstance.updatePersonaStatusUsingPOST(request);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PersonaresourceApi#updatePersonaStatusUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**PersonaRequest**](PersonaRequest.md)| request |

### Return type

**Integer**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

