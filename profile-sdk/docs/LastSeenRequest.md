
# LastSeenRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | [**DateTime**](DateTime.md) |  |  [optional]
**email** | **String** |  |  [optional]



