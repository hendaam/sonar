
# ProfileImageResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  |  [optional]
**orginalImagePath** | **String** |  |  [optional]
**statusCode** | **String** |  |  [optional]
**thumbImagePath** | **String** |  |  [optional]



