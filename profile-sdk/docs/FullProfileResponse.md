
# FullProfileResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**personas** | [**List&lt;PersonaResponse&gt;**](PersonaResponse.md) |  |  [optional]
**profile** | [**Profile**](Profile.md) |  |  [optional]



