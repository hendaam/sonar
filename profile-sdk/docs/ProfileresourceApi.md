# ProfileresourceApi

All URIs are relative to *https://localhost:8082/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteProfileUsingPOST**](ProfileresourceApi.md#deleteProfileUsingPOST) | **POST** /api/profiles/deleteProfile | Delete Profile.
[**getAllProfileBasicInfoUsingGET**](ProfileresourceApi.md#getAllProfileBasicInfoUsingGET) | **GET** /api/profiles/getAllProfileBasicInfo | Get all Users Basic Info By Id
[**getProfileBasicInfoUsingGET**](ProfileresourceApi.md#getProfileBasicInfoUsingGET) | **GET** /api/profiles/getProfileBasicInfo | Get User Basic Info By Id
[**getProfileFullInfoUsingGET**](ProfileresourceApi.md#getProfileFullInfoUsingGET) | **GET** /api/profiles/getUserByEmail | Get Full User Info By Id
[**isVaildProfilePhoneUsingGET**](ProfileresourceApi.md#isVaildProfilePhoneUsingGET) | **GET** /api/profiles/isPhoneExists | Check if phone exists or not
[**isVaildProfileUsingGET**](ProfileresourceApi.md#isVaildProfileUsingGET) | **GET** /api/profiles/isUserExists | Check if user exists or not
[**saveBasicInfoUsingPOST**](ProfileresourceApi.md#saveBasicInfoUsingPOST) | **POST** /api/profiles/saveBasicInfo | Save basic information for profile.
[**saveProfileUsingPOST**](ProfileresourceApi.md#saveProfileUsingPOST) | **POST** /api/profiles/saveProfile | Save Customer profile.
[**updateLastLoginUsingPOST**](ProfileresourceApi.md#updateLastLoginUsingPOST) | **POST** /api/profiles/updateLastLogin | Update last login.
[**updateLastLogoutUsingPOST**](ProfileresourceApi.md#updateLastLogoutUsingPOST) | **POST** /api/profiles/updateLastLogout | Update last logout.
[**updatePasswordUsingPOST**](ProfileresourceApi.md#updatePasswordUsingPOST) | **POST** /api/profiles/updatePassword | Update password.


<a name="deleteProfileUsingPOST"></a>
# **deleteProfileUsingPOST**
> ProfileResponse deleteProfileUsingPOST(email)

Delete Profile.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfileresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfileresourceApi apiInstance = new ProfileresourceApi();
String email = "email_example"; // String | email
try {
    ProfileResponse result = apiInstance.deleteProfileUsingPOST(email);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileresourceApi#deleteProfileUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| email |

### Return type

[**ProfileResponse**](ProfileResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getAllProfileBasicInfoUsingGET"></a>
# **getAllProfileBasicInfoUsingGET**
> List&lt;Profile&gt; getAllProfileBasicInfoUsingGET()

Get all Users Basic Info By Id

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfileresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfileresourceApi apiInstance = new ProfileresourceApi();
try {
    List<Profile> result = apiInstance.getAllProfileBasicInfoUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileresourceApi#getAllProfileBasicInfoUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Profile&gt;**](Profile.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getProfileBasicInfoUsingGET"></a>
# **getProfileBasicInfoUsingGET**
> Profile getProfileBasicInfoUsingGET(email)

Get User Basic Info By Id

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfileresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfileresourceApi apiInstance = new ProfileresourceApi();
String email = "email_example"; // String | email
try {
    Profile result = apiInstance.getProfileBasicInfoUsingGET(email);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileresourceApi#getProfileBasicInfoUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| email |

### Return type

[**Profile**](Profile.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getProfileFullInfoUsingGET"></a>
# **getProfileFullInfoUsingGET**
> FullProfileResponse getProfileFullInfoUsingGET(email)

Get Full User Info By Id

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfileresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfileresourceApi apiInstance = new ProfileresourceApi();
String email = "email_example"; // String | email
try {
    FullProfileResponse result = apiInstance.getProfileFullInfoUsingGET(email);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileresourceApi#getProfileFullInfoUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| email |

### Return type

[**FullProfileResponse**](FullProfileResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="isVaildProfilePhoneUsingGET"></a>
# **isVaildProfilePhoneUsingGET**
> PhoneResponse isVaildProfilePhoneUsingGET(phone)

Check if phone exists or not

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfileresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfileresourceApi apiInstance = new ProfileresourceApi();
String phone = "phone_example"; // String | phone
try {
    PhoneResponse result = apiInstance.isVaildProfilePhoneUsingGET(phone);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileresourceApi#isVaildProfilePhoneUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone** | **String**| phone |

### Return type

[**PhoneResponse**](PhoneResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="isVaildProfileUsingGET"></a>
# **isVaildProfileUsingGET**
> ProfileResponse isVaildProfileUsingGET(email)

Check if user exists or not

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfileresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfileresourceApi apiInstance = new ProfileresourceApi();
String email = "email_example"; // String | email
try {
    ProfileResponse result = apiInstance.isVaildProfileUsingGET(email);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileresourceApi#isVaildProfileUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| email |

### Return type

[**ProfileResponse**](ProfileResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="saveBasicInfoUsingPOST"></a>
# **saveBasicInfoUsingPOST**
> ProfileResponse saveBasicInfoUsingPOST(profileRequest)

Save basic information for profile.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfileresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfileresourceApi apiInstance = new ProfileresourceApi();
ProfileRequest profileRequest = new ProfileRequest(); // ProfileRequest | profileRequest
try {
    ProfileResponse result = apiInstance.saveBasicInfoUsingPOST(profileRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileresourceApi#saveBasicInfoUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profileRequest** | [**ProfileRequest**](ProfileRequest.md)| profileRequest |

### Return type

[**ProfileResponse**](ProfileResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="saveProfileUsingPOST"></a>
# **saveProfileUsingPOST**
> ProfileResponse saveProfileUsingPOST(profileRequest)

Save Customer profile.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfileresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfileresourceApi apiInstance = new ProfileresourceApi();
ProfileRequest profileRequest = new ProfileRequest(); // ProfileRequest | profileRequest
try {
    ProfileResponse result = apiInstance.saveProfileUsingPOST(profileRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileresourceApi#saveProfileUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profileRequest** | [**ProfileRequest**](ProfileRequest.md)| profileRequest |

### Return type

[**ProfileResponse**](ProfileResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="updateLastLoginUsingPOST"></a>
# **updateLastLoginUsingPOST**
> Integer updateLastLoginUsingPOST(lastSeenRequest)

Update last login.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfileresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfileresourceApi apiInstance = new ProfileresourceApi();
LastSeenRequest lastSeenRequest = new LastSeenRequest(); // LastSeenRequest | lastSeenRequest
try {
    Integer result = apiInstance.updateLastLoginUsingPOST(lastSeenRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileresourceApi#updateLastLoginUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lastSeenRequest** | [**LastSeenRequest**](LastSeenRequest.md)| lastSeenRequest |

### Return type

**Integer**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="updateLastLogoutUsingPOST"></a>
# **updateLastLogoutUsingPOST**
> Integer updateLastLogoutUsingPOST(lastSeenRequest)

Update last logout.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfileresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfileresourceApi apiInstance = new ProfileresourceApi();
LastSeenRequest lastSeenRequest = new LastSeenRequest(); // LastSeenRequest | lastSeenRequest
try {
    Integer result = apiInstance.updateLastLogoutUsingPOST(lastSeenRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileresourceApi#updateLastLogoutUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lastSeenRequest** | [**LastSeenRequest**](LastSeenRequest.md)| lastSeenRequest |

### Return type

**Integer**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="updatePasswordUsingPOST"></a>
# **updatePasswordUsingPOST**
> Integer updatePasswordUsingPOST(passwordRequest)

Update password.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfileresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfileresourceApi apiInstance = new ProfileresourceApi();
PasswordRequest passwordRequest = new PasswordRequest(); // PasswordRequest | passwordRequest
try {
    Integer result = apiInstance.updatePasswordUsingPOST(passwordRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileresourceApi#updatePasswordUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **passwordRequest** | [**PasswordRequest**](PasswordRequest.md)| passwordRequest |

### Return type

**Integer**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

