/*
 * Hendaam profile api
 * Profile API manage customers profiles
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.ProfileResponse;
import io.swagger.client.model.Profile;
import io.swagger.client.model.FullProfileResponse;
import io.swagger.client.model.PhoneResponse;
import io.swagger.client.model.ProfileRequest;
import io.swagger.client.model.LastSeenRequest;
import io.swagger.client.model.PasswordRequest;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for ProfileresourceApi
 */
public class ProfileresourceApiTest {

    private final ProfileresourceApi api = new ProfileresourceApi();

    
    /**
     * Delete Profile.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deleteProfileUsingPOSTTest() throws ApiException {
        String email = null;
        // ProfileResponse response = api.deleteProfileUsingPOST(email);

        // TODO: test validations
    }
    
    /**
     * Get all Users Basic Info By Id
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getAllProfileBasicInfoUsingGETTest() throws ApiException {
        // List<Profile> response = api.getAllProfileBasicInfoUsingGET();

        // TODO: test validations
    }
    
    /**
     * Get User Basic Info By Id
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getProfileBasicInfoUsingGETTest() throws ApiException {
        String email = null;
        // Profile response = api.getProfileBasicInfoUsingGET(email);

        // TODO: test validations
    }
    
    /**
     * Get Full User Info By Id
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getProfileFullInfoUsingGETTest() throws ApiException {
        String email = null;
        // FullProfileResponse response = api.getProfileFullInfoUsingGET(email);

        // TODO: test validations
    }
    
    /**
     * Check if phone exists or not
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void isVaildProfilePhoneUsingGETTest() throws ApiException {
        String phone = null;
        // PhoneResponse response = api.isVaildProfilePhoneUsingGET(phone);

        // TODO: test validations
    }
    
    /**
     * Check if user exists or not
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void isVaildProfileUsingGETTest() throws ApiException {
        String email = null;
        // ProfileResponse response = api.isVaildProfileUsingGET(email);

        // TODO: test validations
    }
    
    /**
     * Save basic information for profile.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saveBasicInfoUsingPOSTTest() throws ApiException {
        ProfileRequest profileRequest = null;
        // ProfileResponse response = api.saveBasicInfoUsingPOST(profileRequest);

        // TODO: test validations
    }
    
    /**
     * Save Customer profile.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void saveProfileUsingPOSTTest() throws ApiException {
        ProfileRequest profileRequest = null;
        // ProfileResponse response = api.saveProfileUsingPOST(profileRequest);

        // TODO: test validations
    }
    
    /**
     * Update last login.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void updateLastLoginUsingPOSTTest() throws ApiException {
        LastSeenRequest lastSeenRequest = null;
        // Integer response = api.updateLastLoginUsingPOST(lastSeenRequest);

        // TODO: test validations
    }
    
    /**
     * Update last logout.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void updateLastLogoutUsingPOSTTest() throws ApiException {
        LastSeenRequest lastSeenRequest = null;
        // Integer response = api.updateLastLogoutUsingPOST(lastSeenRequest);

        // TODO: test validations
    }
    
    /**
     * Update password.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void updatePasswordUsingPOSTTest() throws ApiException {
        PasswordRequest passwordRequest = null;
        // Integer response = api.updatePasswordUsingPOST(passwordRequest);

        // TODO: test validations
    }
    
}
