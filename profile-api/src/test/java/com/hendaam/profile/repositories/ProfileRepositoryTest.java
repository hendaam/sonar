package com.hendaam.profile.repositories;

import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.HendaamProfileApplication;
import com.hendaam.profile.model.profile.Profile;
import com.hendaam.profile.repository.profile.ProfileRepository;

/**
 * InvitationRepositoryTest for class InvitationRepository
 *
 * @author Simon Ghobreil
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HendaamProfileApplication.class)
@Ignore
public class ProfileRepositoryTest {

    @Autowired
    private ProfileRepository profileRepository;

    @Test
    public void test_vaild_user() {

        Profile profile = profileRepository.findByEmail("semonsamir@gmail.com");

        assertThat(profile, notNullValue());
    }

}
