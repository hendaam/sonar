package com.hendaam.profile.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.HendaamProfileApplication;
import com.hendaam.profile.endpoints.profile.ProfileResource;
import com.hendaam.profile.model.profile.Profile;
import com.hendaam.profile.repository.profile.ProfileRepository;

/**
 * Testing for custom repository.
 *
 * @author Simon Ghobreil.
 */
@SpringApplicationConfiguration(classes = HendaamProfileApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Ignore
public class ProfileCustomRepositoryTest {

    private MockMvc mockMvc;

    @InjectMocks
    private ProfileResource profileControllerTest;

    @Mock
    private ProfileRepository profileRepository;

    @Before
    public void setUp() {
        // Process mock annotations
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(profileControllerTest).build();

    }

    @Test
    public void testIsExistsUser_success_Vaild() throws Exception {

        Profile profile = new Profile();
        profile.setEmail("semonsamir@gmail.com");
        profile.setFullName("simon");
        profile.setEnable(true);

        Mockito.when(profileRepository.findByEmail("semonsamir@gmail.com")).thenReturn(profile);

        mockMvc.perform(get("/api/profiles/isUserExists").param("email", "semonsamir@gmail.com"))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.statusCode", Matchers.equalTo("200")));

    }

    @Test
    public void testIsExistsUser_fail() throws Exception {

        Mockito.when(profileRepository.findByEmail("semonsamir@gmail.com")).thenReturn(null);

        mockMvc.perform(get("/api/profiles/isUserExists").param("email", "semonsamir@gmail.com"))
                .andExpect(status().isOk()).andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.statusCode", Matchers.equalTo("404")));

    }

}
