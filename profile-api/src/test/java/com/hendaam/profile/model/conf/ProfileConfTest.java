package com.hendaam.profile.model.conf;

import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.HendaamProfileApplication;
import com.hendaam.profile.model.conf.profile.PersonaLimitConf;

/**
 * InvitationRepositoryTest for class InvitationRepository
 *
 * @author Simon Ghobreil
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HendaamProfileApplication.class)
@Ignore
public class ProfileConfTest {

    @Autowired
    private PersonaLimitConf profileConf;

    @Test
    public void test_persona_exceed_enable() {

        assertThat(profileConf.getPersonaLimitEnable(), notNullValue());
        assertEquals(profileConf.getPersonaLimitEnable(), false);
    }

}
