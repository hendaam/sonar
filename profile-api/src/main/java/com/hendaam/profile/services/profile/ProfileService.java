package com.hendaam.profile.services.profile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hendaam.profile.model.persona.Persona;
import com.hendaam.profile.model.persona.PersonaStatus;
import com.hendaam.profile.model.persona.StylistPreferences;
import com.hendaam.profile.model.profile.PasswordResetToken;
import com.hendaam.profile.model.profile.Profile;
import com.hendaam.profile.model.profile.VerificationToken;
import com.hendaam.profile.model.vo.profile.FullProfileResponse;
import com.hendaam.profile.model.vo.profile.PasswordRequest;
import com.hendaam.profile.model.vo.profile.ProfileRequest;
import com.hendaam.profile.repository.profile.PasswordResetTokenRepository;
import com.hendaam.profile.repository.profile.ProfileRepository;
import com.hendaam.profile.repository.profile.VerificationTokenRepository;
import com.hendaam.profile.services.notifications.EmailService;
import com.hendaam.profile.services.persona.IPersonaService;

/**
 * @author Simon Ghobreil.
 */
@Service
public class ProfileService {

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private PasswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private IPersonaService personaService;

    @Transactional
    public void saveProfileWithPreferences(ProfileRequest profileRequest) throws Exception {

        Profile profile = profileRequest.getProfile();

        profile.setJoinDate(new Date());

        // 1- Create new persona's
        Persona persona = new Persona();
        persona.setPersonaName("persona1");
        persona.setProfile(profile);

        // 2- Assign preferences to persona's
        handleStylistPreferences(profileRequest, profile, persona);

        // 3- Save Profile
        profileRepository.save(profile);

        // 4- Set Verification token
        String token = UUID.randomUUID().toString();
        VerificationToken verificationToken = new VerificationToken(token, profile);
        verificationTokenRepository.save(verificationToken);

        // 5- Send Notification Email
        emailService.sendConfirmationEmail(profile, token);
        emailService.sendConfirmationEmailToSubscribers(profile);

    }

    /**
     * @param profileRequest
     * @param profile
     * @param persona
     */
    private void handleStylistPreferences(ProfileRequest profileRequest, Profile profile, Persona persona) {
        List<StylistPreferences> stylistPreferencesList = profileRequest.getStylistPreferences();
        if (stylistPreferencesList != null) {
            for (StylistPreferences stylistPreferences : stylistPreferencesList) {
                stylistPreferences.setPersona(persona);
            }
            persona.setStylistPreferences(stylistPreferencesList);
            persona.setDefaultPersona(true);
            persona.setCompleted(false);
            persona.setPersonaStatus(PersonaStatus.INCOMPLETE);
            persona.setPersonaGender(profile.getGender());

            // 3- Assign person's to profile
            List<Persona> personas = new ArrayList<Persona>();
            personas.add(persona);
            profile.setPersonas(personas);
        }
    }

    public void saveBasicInfo(ProfileRequest profileRequest) throws Exception {

        // 1- Add join date
        Profile profile = profileRequest.getProfile();
        profile.setJoinDate(new Date());
        profileRepository.save(profile);

        // 2- Set Verification token
        String token = UUID.randomUUID().toString();
        VerificationToken verificationToken = new VerificationToken(token, profile);
        verificationTokenRepository.save(verificationToken);

        // 3- Send Notification Email
        emailService.sendConfirmationEmail(profile, token);

    }

    public Integer updatePassword(PasswordRequest passwordRequest) throws Exception {
        PasswordResetToken token = passwordResetTokenRepository.findByToken(passwordRequest.getToken());

        Profile profile = token.getUser();

        Integer response = profileRepository.updatePassword(passwordRequest.getPassword(), profile.getEmail());

        emailService.sendResetPasswordConfrimEmail(profile);

        return response;
    }

    public FullProfileResponse getProfileByEmail(String email) throws Exception {
        Profile profile = profileRepository.findByEmail(email);

        FullProfileResponse fullProfileResponse = new FullProfileResponse();

        fullProfileResponse.setProfile(profile);
        fullProfileResponse.setPersonas(personaService.getAllPersonas(email).getPersonas());

        return fullProfileResponse;
    }

    public Profile getProfileBasicInfo(String email) throws Exception {
        return profileRepository.findByEmail(email);
    }

    public List<Profile> getAllProfilesBasicInfo() throws Exception {
        return (List<Profile>) profileRepository.findAll();
    }

}
