package com.hendaam.profile.services.profile;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.hendaam.profile.model.profile.ProfileImage;
import com.hendaam.profile.model.vo.profile.ProfileImageRequest;
import com.hendaam.profile.model.vo.profile.ProfileImageResponse;
import com.hendaam.profile.repository.profile.ProfieImageRepository;
import com.hendaam.profile.repository.profile.ProfileRepository;

/**
 * @author Simon Ghobreil.
 */
@Service
public class ProfileImageService {

    @Autowired
    private ProfieImageRepository profieImageRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Transactional(rollbackOn = Exception.class)
    public void saveProfileImageResponse(ProfileImageRequest profileImageRequest) throws DataAccessException {

        setDefaultImageToFalse(profileImageRequest);

        setDefaultImage(profileImageRequest);
    }

    /**
     * @param profileImageRequest
     * @return
     */
    private void setDefaultImage(ProfileImageRequest profileImageRequest) {
        ProfileImage profileImage = new ProfileImage();
        profileImage.setOrgImage(profileImageRequest.getOrginalImagePath());
        profileImage.setThumbImage(profileImageRequest.getThumbImagePath());
        profileImage.setDefaultImage(true);

        profileImage.setUser(profileRepository.findByEmail(profileImageRequest.getEmail()));
        profieImageRepository.save(profileImage);
    }

    /**
     * @param profileImageRequest
     */
    private void setDefaultImageToFalse(ProfileImageRequest profileImageRequest) {
        List<ProfileImage> profileImages = profieImageRepository.getAllImagesByEmail(profileImageRequest.getEmail());

        for (ProfileImage profileImage : profileImages) {
            profileImage.setDefaultImage(false);
        }

        profieImageRepository.save(profileImages);
    }

    @Transactional(rollbackOn = Exception.class)
    public ProfileImageResponse getProfileDefaultImageByEmail(String email) throws DataAccessException {
        ProfileImageResponse profileImageResponse = new ProfileImageResponse();

        ProfileImage profileImage = profieImageRepository.getDefaultImageByEmail(email);

        profileImageResponse.setEmail(email);
        profileImageResponse.setStatusCode("200");
        profileImageResponse.setOrginalImagePath(profileImage.getOrgImage());
        profileImageResponse.setThumbImagePath(profileImage.getThumbImage());
        return profileImageResponse;
    }
}
