package com.hendaam.profile.services.notifications;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.hendaam.notification.sdk.api.ProfileendpointApi;
import com.hendaam.notification.sdk.client.ApiClient;
import com.hendaam.notification.sdk.client.ApiException;
import com.hendaam.notification.sdk.model.EmailRequest;
import com.hendaam.notification.sdk.model.HendaamEmailProperties;
import com.hendaam.profile.model.conf.restclient.RestClientConfig;
import com.hendaam.profile.model.profile.Profile;

/**
 * @author Simon Ghobreil.
 */
@Component
public class EmailService {

    @Autowired
    private RestClientConfig restClient;

    @Async
    public void sendConfirmationEmail(Profile profile, String token) {

        try {
            ProfileendpointApi profileEndpoint = getAppointmentApiClient();
            List<HendaamEmailProperties> emailProperties = new ArrayList<HendaamEmailProperties>();

            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setSendTo(profile.getEmail());
            emailRequest.setEmailProperties(emailProperties);
            emailProperties.add(createHendaamProperties("token", token));
            emailRequest.setEmailProperties(emailProperties);

            profileEndpoint.sendConfirmedEmailUsingPOST(emailRequest);

        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    @Async
    public void sendConfirmedEmail(Profile profile) {

        try {
            ProfileendpointApi profileEndpoint = getAppointmentApiClient();
            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setSendTo(profile.getEmail());

            profileEndpoint.sendConfirmedEmailUsingPOST(emailRequest);

        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    @Async
    public void sendConfirmationEmailToSubscribers(Profile profile) {

        try {
            ProfileendpointApi profileEndpoint = getAppointmentApiClient();
            List<HendaamEmailProperties> emailProperties = new ArrayList<HendaamEmailProperties>();

            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setSendTo(profile.getEmail());
            emailRequest.setEmailProperties(emailProperties);

            emailProperties.add(createHendaamProperties("email", profile.getEmail()));
            emailProperties.add(createHendaamProperties("phone", profile.getPhone()));
            emailProperties.add(createHendaamProperties("name", profile.getFullName()));

            emailRequest.setEmailProperties(emailProperties);

            profileEndpoint.sendRegistrationToSubscribersUsingPOST(emailRequest);
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    @Async
    public void sendResetPasswordEmail(Profile profile, String token) {

        try {
            ProfileendpointApi profileEndpoint = getAppointmentApiClient();
            List<HendaamEmailProperties> emailProperties = new ArrayList<HendaamEmailProperties>();

            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setSendTo(profile.getEmail());

            emailProperties.add(createHendaamProperties("token", token));
            emailRequest.setEmailProperties(emailProperties);

            profileEndpoint.sendResetPasswordUsingPOST(emailRequest);
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    @Async
    public void sendResetPasswordConfrimEmail(Profile profile) {

        try {

            ProfileendpointApi profileEndpoint = getAppointmentApiClient();

            EmailRequest emailRequest = new EmailRequest();
            emailRequest.setSendTo(profile.getEmail());

            profileEndpoint.sendResetPasswordConfirmationUsingPOST(emailRequest);

        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    private ProfileendpointApi getAppointmentApiClient() {
        ApiClient apiClient = new ApiClient();
        apiClient.setUsername(restClient.getNotificationUserName());
        apiClient.setPassword(restClient.getNotificationPassword());
        apiClient.setBasePath(restClient.getNotificationServerURL());

        return new ProfileendpointApi(apiClient);
    }

    /**
     * @param token
     * @return
     */
    private HendaamEmailProperties createHendaamProperties(String key, String value) {
        HendaamEmailProperties properties = new HendaamEmailProperties();
        properties.setKey(key);
        properties.setValue(value);
        return properties;
    }

}
