package com.hendaam.profile.services.persona;

import com.hendaam.profile.model.vo.persona.PersonaRequest;
import com.hendaam.profile.model.vo.persona.PersonaRequestWrapper;
import com.hendaam.profile.model.vo.persona.PersonaResponse;

public interface IPersonaService {

    PersonaResponse createNewPersonaWithPreferences(PersonaRequest personaRequest);

    PersonaResponse createNewPersonaWithoutPreferences(PersonaRequest personaRequest);

    PersonaResponse getDefaultPersona(String email);

    PersonaRequestWrapper getAllPersonas(String email);

    Integer updatePersonaStatus(PersonaRequest personaRequest);

    PersonaResponse changePersonaToDefault(PersonaRequest personaRequest);

}
