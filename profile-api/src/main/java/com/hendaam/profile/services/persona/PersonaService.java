package com.hendaam.profile.services.persona;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hendaam.profile.model.persona.Persona;
import com.hendaam.profile.model.persona.PersonaStatus;
import com.hendaam.profile.model.persona.StylistPreferences;
import com.hendaam.profile.model.vo.persona.AbstractStylistPreferences;
import com.hendaam.profile.model.vo.persona.PersonaRequest;
import com.hendaam.profile.model.vo.persona.PersonaRequestWrapper;
import com.hendaam.profile.model.vo.persona.PersonaResponse;
import com.hendaam.profile.repository.persona.PersonaRepository;
import com.hendaam.profile.repository.profile.ProfileRepository;


/**
 * @author Simon Ghobreil.
 */
@Service
public class PersonaService implements IPersonaService {

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public PersonaResponse createNewPersonaWithPreferences(PersonaRequest personaRequest) {

        PersonaResponse personaResponse = new PersonaResponse();

        try {

            // 1- Create new persona's
            Persona persona = new Persona();
            persona.setProfile(profileRepository.findByEmail(personaRequest.getEmail()));

            // 2- Assign preferences to persona's
            List<AbstractStylistPreferences> stylistPreferencesList = personaRequest.getStylistPreferences();

            List<com.hendaam.profile.model.persona.StylistPreferences> persistedList = new ArrayList<com.hendaam.profile.model.persona.StylistPreferences>();
            for (AbstractStylistPreferences stylistPreferences : stylistPreferencesList) {
                com.hendaam.profile.model.persona.StylistPreferences persistedRref = new com.hendaam.profile.model.persona.StylistPreferences();
                persistedRref.setAnswer(stylistPreferences.getAnswer());
                persistedRref.setQuestion(stylistPreferences.getQuestion());
                persistedRref.setPersona(persona);

                persistedList.add(persistedRref);
            }

            persona.setStylistPreferences(persistedList);
            persona.setCompleted(personaRequest.getCompleted());
            persona.setPersonaStatus(PersonaStatus.COMPLETED);
            persona.setDefaultPersona(personaRequest.getDefaultPersona());
            persona.setPersonaGender(personaRequest.getPersonaGender());
            if (personaRequest.getPersonaName() != null) {
                persona.setPersonaName(personaRequest.getPersonaName());
            } else {
                persona.setPersonaName("persona");
            }

            personaRepository.save(persona);

            personaResponse.setEmail(personaRequest.getEmail());
            personaResponse.setStatusCode("200");
            personaResponse.setPersonaName(persona.getPersonaName());
            personaResponse.setDefaultPersona(persona.getDefaultPersona());

        } catch (Exception exception) {
            personaResponse.setEmail(personaRequest.getEmail());
            personaResponse.setStatusCode("204");
        }

        return personaResponse;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public PersonaResponse createNewPersonaWithoutPreferences(PersonaRequest personaRequest) {

        PersonaResponse personaResponse = new PersonaResponse();

        try {

            // 1- Create new persona's
            Persona persona = new Persona();
            persona.setProfile(profileRepository.findByEmail(personaRequest.getEmail()));

            persona.setCompleted(personaRequest.getCompleted());
            persona.setDefaultPersona(personaRequest.getDefaultPersona());
            if (!personaRequest.getPersonaName().isEmpty()) {
                persona.setPersonaName(personaRequest.getPersonaName());
            } else {
                persona.setPersonaName("persona");
            }

            personaRepository.save(persona);

            personaResponse.setEmail(personaRequest.getEmail());
            personaResponse.setStatusCode("200");
            personaResponse.setPersonaName(persona.getPersonaName());
            personaResponse.setDefaultPersona(persona.getDefaultPersona());

        } catch (Exception exception) {
            personaResponse.setEmail(personaRequest.getEmail());
            personaResponse.setStatusCode("204");
        }

        return personaResponse;
    }

    @Override
    @Transactional
    public PersonaResponse getDefaultPersona(String email) {

        PersonaResponse personaResponse = new PersonaResponse();

        Persona persona = personaRepository.findAllByDefaultPersona(email);

        if (persona == null) {
            personaResponse.setStatusCode("404");
            personaResponse.setEmail(email);
        } else {
            personaResponse.setStatusCode("200");
            personaResponse.setCompleted(persona.getCompleted());
            personaResponse.setDefaultPersona(persona.getDefaultPersona());
            personaResponse.setEmail(email);
            personaResponse.setPersonaName(persona.getPersonaName());
            personaResponse.setPersonaStatus(PersonaStatus.valueOf(persona.getPersonaStatus().toString()));
            personaResponse.setPersonaGender(persona.getPersonaGender());
            personaResponse.setStylistPreferences(convertStylistPreferences(persona.getStylistPreferences()));
        }
        return personaResponse;
    }

    @Override
    @Transactional
    public PersonaRequestWrapper getAllPersonas(String email) {

        PersonaRequestWrapper personaRequestWrapper = new PersonaRequestWrapper();

        List<PersonaResponse> personaResponses = new ArrayList<PersonaResponse>();

        try {

            List<Persona> personas = personaRepository.findAllByProfile(email);

            PersonaResponse personaResponse = null;
            for (Persona persona : personas) {
                personaResponse = new PersonaResponse(convertStylistPreferences(persona.getStylistPreferences()), persona.getPersonaName(), persona.getCompleted(), persona.getDefaultPersona(),
                        PersonaStatus.valueOf(persona.getPersonaStatus().toString()), persona.getPersonaGender());

                personaResponse.setStatusCode("200");
                personaResponse.setId(persona.getPersonaId());

                personaResponses.add(personaResponse);

            }

        } catch (Exception exception) {

        }
        personaRequestWrapper.setPersonas(personaResponses);
        return personaRequestWrapper;
    }

    @Override
    @Transactional(value = TxType.REQUIRED, rollbackOn = Exception.class)
    public Integer updatePersonaStatus(PersonaRequest personaRequest) {

        try {
            Persona persona = personaRepository.findAllByDefaultPersona(personaRequest.getEmail());

            if (personaRequest.getStylistPreferences() != null) {
                persona.setCompleted(personaRequest.getCompleted());
                persona.setDefaultPersona(personaRequest.getDefaultPersona());
                persona.setPersonaStatus(PersonaStatus.valueOf(personaRequest.getPersonaStatus().toString()));
                persona.getStylistPreferences().addAll(convertToStylistPreferences(personaRequest.getStylistPreferences(), persona));
            } else {
                persona.setPersonaStatus(PersonaStatus.valueOf(personaRequest.getPersonaStatus().toString()));
            }

            personaRepository.save(persona);

            return 1;
        } catch (Exception exception) {
            return 0;
        }
    }

    @Override
    @Transactional(value = TxType.REQUIRED, rollbackOn = Exception.class)
    public PersonaResponse changePersonaToDefault(PersonaRequest personaRequest) {
        PersonaResponse personaResponse = new PersonaResponse();

        Long id = new Long(personaRequest.getId());

        try {

            List<Persona> personas = personaRepository.findAllByProfile(personaRequest.getEmail());

            for (Persona persona : personas) {
                if (persona.getPersonaId() == personaRequest.getId()) {
                    persona.setDefaultPersona(true);
                } else {
                    persona.setDefaultPersona(false);
                }
            }

            personaRepository.save(personas);

            personaResponse.setId(id);
            personaResponse.setStatusCode("200");
            personaResponse.setEmail(personaRequest.getEmail());

        } catch (Exception exception) {
            personaResponse.setId(id);
            personaResponse.setStatusCode("204");
        }

        return personaResponse;
    }

    private List<AbstractStylistPreferences> convertStylistPreferences(List<com.hendaam.profile.model.persona.StylistPreferences> preferences) {

        List<AbstractStylistPreferences> newStylistPreferences = new ArrayList<AbstractStylistPreferences>();

        for (com.hendaam.profile.model.persona.StylistPreferences stylistPreferences : preferences) {
            newStylistPreferences.add(new AbstractStylistPreferences(stylistPreferences.getQuestion(), stylistPreferences.getAnswer()));
        }
        return newStylistPreferences;
    }

    private List<StylistPreferences> convertToStylistPreferences(List<AbstractStylistPreferences> preferences, Persona persona) {

        List<StylistPreferences> stylistPreferencesList = new ArrayList<StylistPreferences>();

        StylistPreferences newStylistPreferences = null;
        for (AbstractStylistPreferences stylistPreferences : preferences) {
            newStylistPreferences = new StylistPreferences();
            newStylistPreferences.setQuestion(stylistPreferences.getQuestion());
            newStylistPreferences.setAnswer(stylistPreferences.getAnswer());
            newStylistPreferences.setPersona(persona);
            stylistPreferencesList.add(newStylistPreferences);
        }
        return stylistPreferencesList;
    }

}
