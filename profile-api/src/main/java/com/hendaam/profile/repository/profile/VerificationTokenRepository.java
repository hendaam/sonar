package com.hendaam.profile.repository.profile;

import org.springframework.data.repository.CrudRepository;

import com.hendaam.profile.model.profile.Profile;
import com.hendaam.profile.model.profile.VerificationToken;

/**
 * This is profile repository.
 *
 * @author Simon Ghobreil.
 */
public interface VerificationTokenRepository extends CrudRepository<VerificationToken, Integer> {

    VerificationToken findByProfile(Profile profile);

    VerificationToken findByToken(String token);

}
