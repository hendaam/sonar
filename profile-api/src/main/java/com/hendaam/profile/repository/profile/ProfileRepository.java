package com.hendaam.profile.repository.profile;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.hendaam.profile.model.profile.Profile;

/**
 * This is profile repository.
 *
 * @author Simon Ghobreil.
 */
public interface ProfileRepository extends CrudRepository<Profile, Integer> {

    Profile findByEmail(@Param("email") String email);

    Profile findByPhone(@Param("phone") String phone);

    @Modifying
    @Query("update Profile profile set profile.passwordHash = ?1 where profile.email = ?2")
    @Transactional
    Integer updatePassword(String password, String email);

    @Modifying
    @Query("update Profile profile set profile.lastLoginTime = ?1 where profile.email = ?2")
    @Transactional
    Integer updatelastLoginTime(Date loginDate, String email);

    @Modifying
    @Query("update Profile profile set profile.lastLogoutTime = ?1 where profile.email = ?2")
    @Transactional
    Integer updatelastLogoutTime(Date logoutDate, String email);

}
