package com.hendaam.profile.repository.profile;

import org.springframework.data.repository.CrudRepository;

import com.hendaam.profile.model.profile.PasswordResetToken;
import com.hendaam.profile.model.profile.Profile;

/**
 * This is profile repository.
 *
 * @author Simon Ghobreil.
 */
public interface PasswordResetTokenRepository extends CrudRepository<PasswordResetToken, Integer> {

    PasswordResetToken findByToken(String token);

    PasswordResetToken findByUser(Profile profile);
}
