package com.hendaam.profile.repository.profile;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hendaam.profile.model.profile.Profile;
import com.hendaam.profile.model.profile.ProfileImage;

/**
 * This is profile repository.
 *
 * @author Simon Ghobreil.
 */
public interface ProfieImageRepository extends CrudRepository<ProfileImage, Long> {

    ProfileImage findByUser(Profile profile);

    @Query("from ProfileImage profileImage where profileImage.user.email = ?1 and profileImage.defaultImage = 1")
    @Transactional
    ProfileImage getDefaultImageByEmail(String email);

    @Query("from ProfileImage profileImage where profileImage.user.email = ?1")
    @Transactional
    List<ProfileImage> getAllImagesByEmail(String email);

}
