package com.hendaam.profile.repository.persona;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.hendaam.profile.model.persona.StylistPreferences;

/**
 * This is stylist preferences repository.
 *
 * @author Simon Ghobreil.
 */
@RepositoryRestResource
public interface StylistPreferencesRepository extends CrudRepository<StylistPreferences, Integer> {

}
