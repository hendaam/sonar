package com.hendaam.profile.repository.persona;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.hendaam.profile.model.persona.Persona;

/**
 * This is persona repository.
 *
 * @author Simon Ghobreil.
 */
public interface PersonaRepository extends CrudRepository<Persona, Long> {

    @Query("from Persona persona where persona.profile.email = ?1")
    List<Persona> findAllByProfile(String email);

    @Query("from Persona persona where persona.profile.email = ?1 and persona.defaultPersona = 1")
    Persona findAllByDefaultPersona(String email);

}
