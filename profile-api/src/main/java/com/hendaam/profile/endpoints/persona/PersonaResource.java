package com.hendaam.profile.endpoints.persona;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hendaam.profile.model.vo.persona.PersonaRequest;
import com.hendaam.profile.model.vo.persona.PersonaRequestWrapper;
import com.hendaam.profile.model.vo.persona.PersonaResponse;
import com.hendaam.profile.services.persona.IPersonaService;

import io.swagger.annotations.Api;

/**
 * Profile custom repository which will had all custom methods.
 *
 * @author Simon Ghobreil.
 */
@RestController
@RequestMapping("/api/persona")
@Api(value = "Persona Resource", produces = "application/json")
@Configuration
public class PersonaResource {

    @Autowired
    private IPersonaService personaService;

    @RequestMapping(method = RequestMethod.POST, path = "/createPersonaWithPreferences")
    public @ResponseBody PersonaResponse createPersonaWithPreferences(@RequestBody PersonaRequest request) {
        return personaService.createNewPersonaWithPreferences(request);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/createPersonaWithoutPreferences")
    public @ResponseBody PersonaResponse createPersonaWithoutPreferences(@RequestBody PersonaRequest request) {
        return personaService.createNewPersonaWithoutPreferences(request);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getAllPersonas")
    public @ResponseBody PersonaRequestWrapper getAllPersonas(@RequestParam(value = "email") String email) {
        return personaService.getAllPersonas(email);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getDefaultPersona")
    public @ResponseBody PersonaResponse getDefaultPersona(@RequestParam(value = "email") String email) {
        return personaService.getDefaultPersona(email);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/updateDefaultPersona")
    public @ResponseBody Integer updatePersonaStatus(@RequestBody PersonaRequest request) {
        return personaService.updatePersonaStatus(request);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/changePersonaToDefault")
    public @ResponseBody PersonaResponse changePersonaToDefault(@RequestBody PersonaRequest request) {

        return personaService.changePersonaToDefault(request);
    }
}
