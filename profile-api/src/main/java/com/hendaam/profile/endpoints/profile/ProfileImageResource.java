package com.hendaam.profile.endpoints.profile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hendaam.profile.model.vo.profile.ProfileImageRequest;
import com.hendaam.profile.model.vo.profile.ProfileImageResponse;
import com.hendaam.profile.services.profile.ProfileImageService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Profile image resource.
 *
 * @author Simon Ghobreil.
 */
@RestController
@RequestMapping("/api/profiles/image")
@Api(value = "Profile Image Resource", produces = "application/json")
@Configuration
public class ProfileImageResource {

    private final Logger log = LoggerFactory.getLogger(ProfileImageResource.class);

    @Autowired
    private ProfileImageService profileImageService;

    @ApiOperation(value = "Save profile image")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Everything working fine"),
                            @ApiResponse(code = 500, message = "Somthing went wrong!"), })
    @RequestMapping(method = RequestMethod.POST, path = "/saveProfileImages")
    public @ResponseBody ResponseEntity<ProfileImageResponse> saveProfileImages(@RequestBody ProfileImageRequest profileImageRequest) {

        log.debug("REST request to save profile images : {}", profileImageRequest.getEmail() + profileImageRequest.getOrginalImagePath());

        ProfileImageResponse profileImageResponse = new ProfileImageResponse();
        try {

            profileImageService.saveProfileImageResponse(profileImageRequest);
            profileImageResponse.setEmail(profileImageRequest.getEmail());
            profileImageResponse.setStatusCode("200");
            return new ResponseEntity<ProfileImageResponse>(profileImageResponse, HttpStatus.OK);

        } catch (DataAccessException exception) {
            log.error("Something went wrong while save new image : ", exception);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception exception) {
            log.error("Something went wrong : ", exception);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Save profile image")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Everything working fine"),
                            @ApiResponse(code = 500, message = "Somthing went wrong!"), })
    @RequestMapping(method = RequestMethod.GET, path = "/getProfileImages")
    public @ResponseBody ResponseEntity<ProfileImageResponse> getProfileDefaultImageByEmail(@RequestParam(value = "email") String email) {

        log.debug("REST request to save profile images : {}", email);

        try {
            return new ResponseEntity<ProfileImageResponse>(profileImageService.getProfileDefaultImageByEmail(email), HttpStatus.OK);

        } catch (DataAccessException exception) {
            log.error("Something went wrong while get user image : ", exception);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception exception) {
            log.error("Something went wrong in general : ", exception);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
