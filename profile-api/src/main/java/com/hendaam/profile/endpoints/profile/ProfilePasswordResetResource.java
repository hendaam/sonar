package com.hendaam.profile.endpoints.profile;

import java.util.Calendar;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hendaam.profile.model.profile.PasswordResetToken;
import com.hendaam.profile.model.profile.Profile;
import com.hendaam.profile.model.profile.VerificationToken;
import com.hendaam.profile.model.vo.notification.EmailResponse;
import com.hendaam.profile.model.vo.token.PasswordTokenResponse;
import com.hendaam.profile.model.vo.token.TokenRequest;
import com.hendaam.profile.model.vo.token.TokenStatusResponse;
import com.hendaam.profile.repository.profile.PasswordResetTokenRepository;
import com.hendaam.profile.repository.profile.ProfileRepository;
import com.hendaam.profile.repository.profile.VerificationTokenRepository;
import com.hendaam.profile.services.notifications.EmailService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Simon Ghobreil.
 */
@RestController
@RequestMapping("/api/token")
@Api(value = "Profile Password Reset Resource", produces = "application/json")
@Configuration
public class ProfilePasswordResetResource {

    private final Logger log = LoggerFactory.getLogger(ProfilePasswordResetResource.class);

    @Autowired
    private PasswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;


    /**
     * This method check if profile exists or not.
     *
     * @param email
     *        String
     * @return profile response with will contain response code with email.
     */
    @ApiOperation(value = "Send Reset password token")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Password reset token sent successifully"), @ApiResponse(code = 404, message = "Email not found"), @ApiResponse(code = 500, message = "Somthing went wrong!") })
    @RequestMapping(method = RequestMethod.GET, path = "/sendResetPasswordToken")
    public @ResponseBody ResponseEntity<TokenStatusResponse> sendResetPasswordToken(@RequestParam(value = "email") String email) {

        log.debug("REST request to send reset password token : {}", email);

        try {

            TokenStatusResponse tokenResponse = new TokenStatusResponse();

            final String token = UUID.randomUUID().toString();
            Profile profile = profileRepository.findByEmail(email);

            if (profile != null) {
                PasswordResetToken passwordResetToken = new PasswordResetToken(token, profile);
                passwordResetTokenRepository.save(passwordResetToken);
                emailService.sendResetPasswordEmail(profile, token);

                tokenResponse.setStatusCode("200");
                return new ResponseEntity<TokenStatusResponse>(tokenResponse, HttpStatus.OK);

            } else {
                tokenResponse.setStatusCode("404");
                return new ResponseEntity<TokenStatusResponse>(tokenResponse, HttpStatus.NOT_FOUND);
            }

        } catch (Exception exception) {
            log.error("Something went wrong while send reset password token : ", exception);
            return new ResponseEntity<TokenStatusResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * This method check if profile exists or not.
     *
     * @param email
     *        String
     * @return profile response with will contain response code with email.
     */
    @RequestMapping(method = RequestMethod.GET, path = "/isVaildPasswordToken")
    public @ResponseBody ResponseEntity<PasswordTokenResponse> isVaildToken(@RequestParam(value = "token") String token) {

        log.debug("REST request to check if password token is vaild : {}", token);

        try {

            PasswordTokenResponse tokenResponse = new PasswordTokenResponse();
            tokenResponse.setToken(token);

            PasswordResetToken passwordResetToken = passwordResetTokenRepository.findByToken(token);

            if (passwordResetToken == null) {
                tokenResponse.setStatusCode("404");
                return new ResponseEntity<PasswordTokenResponse>(tokenResponse, HttpStatus.NOT_FOUND);
            }

            Calendar cal = Calendar.getInstance();
            if ((passwordResetToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
                tokenResponse.setStatusCode("405");
                return new ResponseEntity<PasswordTokenResponse>(tokenResponse, HttpStatus.METHOD_NOT_ALLOWED);
            }

            tokenResponse.setStatusCode("200");
            return new ResponseEntity<PasswordTokenResponse>(tokenResponse, HttpStatus.OK);



        } catch (Exception exception) {
            log.error("Something went wrong while check password token : ", exception);
            return new ResponseEntity<PasswordTokenResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This method check if profile exists or not.
     *
     * @param email
     *        String
     * @return profile response with will contain response code with email.
     */
    @RequestMapping(method = RequestMethod.POST, path = "/updateVerificationToken")
    public @ResponseBody EmailResponse updateVerificationToken(@RequestBody TokenRequest request) {

        EmailResponse emailResponse = new EmailResponse();
        emailResponse.setToken(request.getToken());

        VerificationToken verificationToken = verificationTokenRepository.findByToken(request.getToken());

        if (verificationToken == null) {
            emailResponse.setStatusCode("404");
            return emailResponse;
        }

        Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            emailResponse.setStatusCode("405");
            return emailResponse;
        }

        if (verificationToken.getVerified()) {
            emailResponse.setStatusCode("204");
            return emailResponse;
        }

        Profile profile = verificationToken.getProfile();
        profile.setEnable(true);
        profileRepository.save(profile);

        verificationToken.setVerified(true);
        verificationTokenRepository.save(verificationToken);

        // 5- Send Notification Email
        emailService.sendConfirmedEmail(profile);

        emailResponse.setStatusCode("200");

        // TODO catch DAO Exception

        return emailResponse;
    }

    /**
     * This method check if profile exists or not.
     *
     * @param email
     *        String
     * @return profile response with will contain response code with email.
     */
    @RequestMapping(method = RequestMethod.POST, path = "/resendVerificationToken")
    public @ResponseBody EmailResponse resendVerificationToken(@RequestBody TokenRequest request) {

        EmailResponse emailResponse = new EmailResponse();
        emailResponse.setToken(request.getToken());

        VerificationToken verificationToken = verificationTokenRepository.findByToken(request.getToken());
        verificationToken.updateToken(UUID.randomUUID().toString());
        verificationTokenRepository.save(verificationToken);

        // 5- Send Notification Email
        emailService.sendConfirmationEmail(verificationToken.getProfile(), verificationToken.getToken());

        emailResponse.setStatusCode("200");

        // TODO catch DAO Exception
        return emailResponse;
    }
}
