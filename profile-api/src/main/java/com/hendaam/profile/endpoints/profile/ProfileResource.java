package com.hendaam.profile.endpoints.profile;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hendaam.profile.model.profile.Profile;
import com.hendaam.profile.model.vo.profile.FullProfileResponse;
import com.hendaam.profile.model.vo.profile.LastSeenRequest;
import com.hendaam.profile.model.vo.profile.PasswordRequest;
import com.hendaam.profile.model.vo.profile.PhoneResponse;
import com.hendaam.profile.model.vo.profile.ProfileRequest;
import com.hendaam.profile.model.vo.profile.ProfileResponse;
import com.hendaam.profile.repository.profile.ProfileRepository;
import com.hendaam.profile.services.profile.ProfileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * REST controller for managing users profile.
 */
@RestController
@RequestMapping("/api/profiles")
@Api(value = "ProfileResource", produces = "application/json")
@Configuration
public class ProfileResource {

    private final Logger log = LoggerFactory.getLogger(ProfileResource.class);

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private ProfileService profileService;

    /**
     * This method check if profile exists or not.
     *
     * @param email
     *        String
     * @return profile response with will contain response code with email.
     */
    @ApiOperation(value = "Check if user exists or not")
    @ApiResponses(value = { @ApiResponse(code = 204, message = "There is no results"), @ApiResponse(code = 500, message = "Somthing went wrong!"),
            @ApiResponse(code = 404, message = "If User not found!"), @ApiResponse(code = 200, message = "If User found and not locked"),
            @ApiResponse(code = 423, message = "If User found and locked") })
    @RequestMapping(method = RequestMethod.GET, path = "/isUserExists")
    public @ResponseBody ResponseEntity<ProfileResponse> isVaildProfile(@RequestParam(value = "email") String email) {

        log.debug("REST request to check if user exists or not : {}", email);

        try {

            Profile profile = profileRepository.findByEmail(email);
            ProfileResponse profileResponse = new ProfileResponse();

            Assert.notNull(profile, "There is no results for this profile");

            if (profile.getEnable()) {
                profileResponse.setStatusCode(HttpStatus.OK.toString());
            } else {
                profileResponse.setStatusCode(HttpStatus.LOCKED.toString());
            }

            profileResponse.setEmail(email);

            return new ResponseEntity<ProfileResponse>(profileResponse, HttpStatus.OK);

        } catch (IllegalArgumentException exception) {
            log.error("Something went wrong while checking for email : ", exception);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception exception) {
            log.error("Something went wrong while checking for email : ", exception);
            return new ResponseEntity<ProfileResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This method check if phone exists or not.
     *
     * @param email
     *        String
     * @return profile response with will contain response code with email.
     */
    @ApiOperation(value = "Check if phone exists or not")
    @ApiResponses(value = { @ApiResponse(code = 204, message = "There is no results"), @ApiResponse(code = 500, message = "Somthing went wrong!"),
            @ApiResponse(code = 404, message = "If User not found!"), @ApiResponse(code = 200, message = "If User found and not locked"),
            @ApiResponse(code = 423, message = "If User found and locked") })
    @RequestMapping(method = RequestMethod.GET, path = "/isPhoneExists")
    public @ResponseBody ResponseEntity<PhoneResponse> isVaildProfilePhone(@RequestParam(value = "phone") String phone) {

        log.debug("REST request to check if phone exists or not : {}", phone);

        try {
            Profile profile = profileRepository.findByPhone(phone);
            PhoneResponse profileResponse = new PhoneResponse();

            Assert.notNull(profile, "There is no results for this profile");

            if (profile == null) {
                profileResponse.setStatusCode("404");
            } else if (profile != null) {
                profileResponse.setStatusCode("200");
            }

            profileResponse.setPhone(phone);

            return new ResponseEntity<PhoneResponse>(profileResponse, HttpStatus.OK);

        } catch (IllegalArgumentException exception) {
            log.error("Something went wrong while checking for phone : ", exception);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception exception) {
            log.error("Something went wrong while checking for phone : ", exception);
            return new ResponseEntity<PhoneResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This method handle save profile with one persona.
     *
     * @param profileRequest
     * @return ProfileResponse.
     */
    @ApiOperation(value = "Save Customer profile.")
    @ApiResponses(value = { @ApiResponse(code = 500, message = "Somthing went wrong!"), @ApiResponse(code = 200, message = "If User found and not locked"),
            @ApiResponse(code = 204, message = "Something wrong happened while save profile") })
    @RequestMapping(method = RequestMethod.POST, path = "/saveProfile")
    public @ResponseBody ResponseEntity<ProfileResponse> saveProfile(@RequestBody ProfileRequest profileRequest) {
        ProfileResponse profileResponse = new ProfileResponse();

        log.debug("REST request to save user profile : {}", profileRequest.getProfile().getEmail());

        try {
            profileService.saveProfileWithPreferences(profileRequest);
            profileResponse.setEmail(profileRequest.getProfile().getEmail());
            profileResponse.setStatusCode("200");

            return new ResponseEntity<ProfileResponse>(profileResponse, HttpStatus.OK);

        } catch (Exception exception) {

            log.error("Something went wrong while save profile : ", exception);
            profileResponse.setEmail(profileRequest.getProfile().getEmail());
            profileResponse.setStatusCode("204");
            return new ResponseEntity<ProfileResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * This method will save basic info ONLY.
     *
     * @param profileRequest
     * @return ProfileResponse
     */
    @ApiOperation(value = "Save basic information for profile.")
    @ApiResponses(value = { @ApiResponse(code = 500, message = "Somthing went wrong!"), @ApiResponse(code = 204, message = "If something wrong happened while save basic info!"),
            @ApiResponse(code = 200, message = "If save successifully happened!") })
    @RequestMapping(method = RequestMethod.POST, path = "/saveBasicInfo")
    public @ResponseBody ResponseEntity<ProfileResponse> saveBasicInfo(@RequestBody ProfileRequest profileRequest) {

        log.debug("REST request to save basic information for profile : {}", profileRequest.getProfile().getEmail());

        ProfileResponse profileResponse = new ProfileResponse();

        try {
            profileService.saveBasicInfo(profileRequest);
            profileResponse.setEmail(profileRequest.getProfile().getEmail());
            profileResponse.setStatusCode("200");

            return new ResponseEntity<ProfileResponse>(profileResponse, HttpStatus.OK);

        } catch (Exception exception) {
            log.error("Something went wrong while save basic info : ", exception);
            profileResponse.setEmail(profileRequest.getProfile().getEmail());
            profileResponse.setStatusCode("204");
            return new ResponseEntity<ProfileResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * This method handle delete profile.
     *
     * @param email
     *        String
     */
    @ApiOperation(value = "Update password.")
    @ApiResponses(value = { @ApiResponse(code = 500, message = "Somthing went wrong!"), @ApiResponse(code = 200, message = "If update successifully happened!") })
    @RequestMapping(method = RequestMethod.POST, path = "/updatePassword")
    public @ResponseBody ResponseEntity<Integer> updatePassword(@RequestBody PasswordRequest passwordRequest) {
        log.debug("REST request to update password : {}", passwordRequest.getToken());

        try {
            return new ResponseEntity<Integer>(profileService.updatePassword(passwordRequest), HttpStatus.OK);
        } catch (Exception exception) {
            log.error("Something went wrong while update password : ", exception);
            return new ResponseEntity<Integer>(0, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This method handle update last login.
     *
     * @param lastSeenRequest
     *        LastSeenRequest
     * @return Integer for determine success or success.
     */
    @ApiOperation(value = "Update last login.")
    @ApiResponses(value = { @ApiResponse(code = 500, message = "Somthing went wrong!"), @ApiResponse(code = 200, message = "If update last login successifully happened!") })
    @RequestMapping(method = RequestMethod.POST, path = "/updateLastLogin")
    public @ResponseBody ResponseEntity<Integer> updateLastLogin(@RequestBody LastSeenRequest lastSeenRequest) {

        log.debug("REST request to update last login : {}", lastSeenRequest.getEmail() + lastSeenRequest.getDate());

        try {
            return new ResponseEntity<Integer>(profileRepository.updatelastLoginTime(lastSeenRequest.getDate(), lastSeenRequest.getEmail()), HttpStatus.OK);
        } catch (Exception exception) {
            log.error("Something went wrong while update last login : ", exception);
            return new ResponseEntity<Integer>(0, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This method handle update last logout.
     *
     * @param lastSeenRequest
     * @return Integer for determine success or success.
     */
    @ApiOperation(value = "Update last logout.")
    @ApiResponses(value = { @ApiResponse(code = 500, message = "Somthing went wrong!"), @ApiResponse(code = 200, message = "If update last logout successifully happened!") })
    @RequestMapping(method = RequestMethod.POST, path = "/updateLastLogout")
    public @ResponseBody ResponseEntity<Integer> updateLastLogout(@RequestBody LastSeenRequest lastSeenRequest) {

        log.debug("REST request to update last logout : {}", lastSeenRequest.getEmail() + lastSeenRequest.getDate());

        try {
            return new ResponseEntity<Integer>(profileRepository.updatelastLogoutTime(lastSeenRequest.getDate(), lastSeenRequest.getEmail()), HttpStatus.OK);
        } catch (Exception exception) {
            log.error("Something went wrong while update last logout : ", exception);
            return new ResponseEntity<Integer>(0, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * This method handle delete profile.
     *
     * @param email
     *        String
     */
    @ApiOperation(value = "Delete Profile.")
    @ApiResponses(value = { @ApiResponse(code = 500, message = "Somthing went wrong!"), @ApiResponse(code = 200, message = "If delete profile successifully happened!") })
    @RequestMapping(method = RequestMethod.POST, path = "/deleteProfile")
    public @ResponseBody ResponseEntity<ProfileResponse> deleteProfile(@RequestParam(value = "email") String email) {

        log.debug("REST request to delete profile : {}", email);

        ProfileResponse profileResponse = new ProfileResponse();

        try {

            Profile profile = profileRepository.findByEmail(email);

            if (profile != null) {
                profileRepository.delete(profile);
                profileResponse.setEmail(email);
                profileResponse.setStatusCode("200");
            } else {
                profileResponse.setEmail(email);
                profileResponse.setStatusCode("404");
            }

            return new ResponseEntity<ProfileResponse>(profileResponse, HttpStatus.OK);

        } catch (Exception exception) {
            log.error("Something went wrong while delete profile: ", exception);
            return new ResponseEntity<ProfileResponse>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    /**
     * This method get profile full information.
     *
     * @param email
     *        String
     */
    @ApiOperation(value = "Get Full User Info By Id")
    @ApiResponses(value = { @ApiResponse(code = 500, message = "Somthing went wrong!"), @ApiResponse(code = 200, message = "If user found!"),
            @ApiResponse(code = 200, message = "If user not found!") })
    @RequestMapping(method = RequestMethod.GET, path = "/getUserByEmail")
    public @ResponseBody ResponseEntity<FullProfileResponse> getProfileFullInfo(@RequestParam(value = "email") String email) {

        log.debug("REST request to get profile : {}", email);

        try {

            return new ResponseEntity<FullProfileResponse>(profileService.getProfileByEmail(email), HttpStatus.OK);

        } catch (Exception exception) {
            log.error("Something went wrong while find profile: ", exception);
            return new ResponseEntity<FullProfileResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * This method get profile full information.
     *
     * @param email
     *        String
     */
    @ApiOperation(value = "Get User Basic Info By Id")
    @ApiResponses(value = { @ApiResponse(code = 500, message = "Somthing went wrong!"), @ApiResponse(code = 200, message = "If user found!"),
            @ApiResponse(code = 200, message = "If user not found!") })
    @RequestMapping(method = RequestMethod.GET, path = "/getProfileBasicInfo")
    public @ResponseBody ResponseEntity<Profile> getProfileBasicInfo(@RequestParam(value = "email") String email) {

        log.debug("REST request to get profile : {}", email);

        try {

            return new ResponseEntity<Profile>(profileService.getProfileBasicInfo(email), HttpStatus.OK);

        } catch (Exception exception) {
            log.error("Something went wrong while find profile: ", exception);
            return new ResponseEntity<Profile>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This method get all profile basic information.
     *
     * @param email
     *        String
     */
    @ApiOperation(value = "Get all Users Basic Info By Id")
    @ApiResponses(value = { @ApiResponse(code = 500, message = "Somthing went wrong!"), @ApiResponse(code = 200, message = "If user found!"),
            @ApiResponse(code = 200, message = "If user not found!") })
    @RequestMapping(method = RequestMethod.GET, path = "/getAllProfileBasicInfo")
    public @ResponseBody ResponseEntity<List<Profile>> getAllProfileBasicInfo() {

        log.debug("REST request to get all profile : {}");

        try {

            return new ResponseEntity<List<Profile>>(profileService.getAllProfilesBasicInfo(), HttpStatus.OK);

        } catch (Exception exception) {
            log.error("Something went wrong while find profile: ", exception);
            return new ResponseEntity<List<Profile>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
