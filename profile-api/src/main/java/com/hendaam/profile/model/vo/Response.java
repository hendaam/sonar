package com.hendaam.profile.model.vo;


/**
 * Base class for all response for profile API.
 * @author Simon Ghobreil.
 */
public class Response {

    private String statusCode;

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode
     *        the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
