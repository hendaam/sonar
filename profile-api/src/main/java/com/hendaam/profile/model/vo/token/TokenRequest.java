package com.hendaam.profile.model.vo.token;

/**
 * @author Simon Ghobreil.
 */
public class TokenRequest extends TokenStatusRequest {

    private String token;

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *        the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

}
