package com.hendaam.profile.model.exception.common;

/**
 * @author Simon Ghobreil.
 */
public class BadCredentialException extends GeneralException {

    private static final long serialVersionUID = 1L;

    public BadCredentialException() {
        super("401");
    }

}
