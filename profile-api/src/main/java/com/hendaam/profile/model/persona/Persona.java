package com.hendaam.profile.model.persona;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hendaam.profile.model.profile.Profile;

/**
 * Persona Entity.
 *
 * @author Simon Ghobreil.
 */
@Entity
@Table(name = "persona")
public class Persona {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "persona_id")
    private Long personaId;

    @Column(name = "persona_name")
    private String personaName;

    @Column(name = "persona_gender")
    private String personaGender;

    @Column(name = "default_persona")
    private Boolean defaultPersona;

    @Column(name = "completed")
    private Boolean completed;

    @Column(name = "persona_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private PersonaStatus personaStatus;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
    private Profile profile;

    @OneToMany(mappedBy = "persona",
               targetEntity = StylistPreferences.class,
               fetch = FetchType.LAZY,
               cascade = CascadeType.ALL)
    private List<StylistPreferences> stylistPreferences;

    /**
     * @return the personaName
     */
    public String getPersonaName() {
        return personaName;
    }

    /**
     * @param personaName
     *        the personaName to set
     */
    public void setPersonaName(String personaName) {
        this.personaName = personaName;
    }

    /**
     * @return the defaultPerson
     */
    public Boolean getDefaultPersona() {
        return defaultPersona;
    }

    /**
     * @param defaultPerson
     *        the defaultPerson to set
     */
    public void setDefaultPersona(Boolean defaultPersona) {
        this.defaultPersona = defaultPersona;
    }

    /**
     * @return the completed
     */
    public Boolean getCompleted() {
        return completed;
    }

    /**
     * @param completed
     *        the completed to set
     */
    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    /**
     * @return the profile
     */
    public Profile getProfile() {
        return profile;
    }

    /**
     * @param profile
     *        the profile to set
     */
    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    /**
     * @return the stylistPreferences
     */
    public List<StylistPreferences> getStylistPreferences() {
        return stylistPreferences;
    }

    /**
     * @param stylistPreferences
     *        the stylistPreferences to set
     */
    public void setStylistPreferences(List<StylistPreferences> stylistPreferences) {
        this.stylistPreferences = stylistPreferences;
    }

    /**
     * @return the personaId
     */
    public Long getPersonaId() {
        return personaId;
    }

    /**
     * @param personaId
     *        the personaId to set
     */
    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    /**
     * @return the personaStatus
     */
    public PersonaStatus getPersonaStatus() {
        return personaStatus;
    }

    /**
     * @param personaStatus
     *        the personaStatus to set
     */
    public void setPersonaStatus(PersonaStatus personaStatus) {
        this.personaStatus = personaStatus;
    }

    /**
     * @return the personaGender
     */
    public String getPersonaGender() {
        return personaGender;
    }

    /**
     * @param personaGender
     *        the personaGender to set
     */
    public void setPersonaGender(String personaGender) {
        this.personaGender = personaGender;
    }

}
