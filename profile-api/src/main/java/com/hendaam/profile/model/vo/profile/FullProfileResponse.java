package com.hendaam.profile.model.vo.profile;

import com.hendaam.profile.model.profile.Profile;
import com.hendaam.profile.model.vo.persona.PersonaRequestWrapper;

/**
 * This is profile response
 *
 * @author Simon Ghobreil
 */
public class FullProfileResponse extends PersonaRequestWrapper {

    private Profile profile;

    /**
     * @return the profile
     */
    public Profile getProfile() {
        return profile;
    }

    /**
     * @param profile
     *        the profile to set
     */
    public void setProfile(Profile profile) {
        this.profile = profile;
    }

}
