package com.hendaam.profile.model.exception.notification;

/**
 * @author Simon Ghobreil.
 */
public class ProfileUnsuccessifulException extends ProfileException {

    private static final long serialVersionUID = 1L;

    public ProfileUnsuccessifulException() {
        super("204");
    }

}
