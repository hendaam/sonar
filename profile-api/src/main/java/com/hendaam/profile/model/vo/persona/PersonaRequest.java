package com.hendaam.profile.model.vo.persona;

import java.util.List;

import com.hendaam.profile.model.persona.PersonaStatus;


public class PersonaRequest {

    private String email;
    private List<AbstractStylistPreferences> stylistPreferences;
    private String personaName;
    private Boolean completed;
    private Boolean defaultPersona;
    private PersonaStatus personaStatus;
    private String personaGender;
    private Long id;

    public PersonaRequest() {
    }

    public PersonaRequest(Long id, Boolean defaultPersona) {
        this.id = id;
        this.defaultPersona = defaultPersona;
    }

    public String getPersonaGender() {
        return personaGender;
    }

    public void setPersonaGender(String personaGender) {
        this.personaGender = personaGender;
    }

    public List<AbstractStylistPreferences> getStylistPreferences() {
        return stylistPreferences;
    }

    public void setStylistPreferences(List<AbstractStylistPreferences> stylistPreferences) {
        this.stylistPreferences = stylistPreferences;
    }

    public String getPersonaName() {
        return personaName;
    }

    public void setPersonaName(String personaName) {
        this.personaName = personaName;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Boolean getDefaultPersona() {
        return defaultPersona;
    }

    public void setDefaultPersona(Boolean defaultPersona) {
        this.defaultPersona = defaultPersona;
    }

    public PersonaStatus getPersonaStatus() {
        return personaStatus;
    }

    public void setPersonaStatus(PersonaStatus personaStatus) {
        this.personaStatus = personaStatus;
    }

    public Long getId() {
        return id;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *        the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
