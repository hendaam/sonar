package com.hendaam.profile.model.vo.profile;

/**
 * @author Simon Ghobreil.
 */
public class PasswordRequest {

    private String password;

    private String token;

    public PasswordRequest() {
    }

    public PasswordRequest(String password, String token) {
        super();
        this.password = password;
        this.token = token;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *        the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *        the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

}
