package com.hendaam.profile.model.profile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "profile_images")
public class ProfileImage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "orginal_img")
    private String orgImage;

    @Column(name = "thumb_img")
    private String thumbImage;

    @OneToOne(targetEntity = Profile.class, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "profile_id")
    private Profile user;

    @Column(name = "default_image")
    private Boolean defaultImage;

    public ProfileImage() {
        super();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *        the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the orgImage
     */
    public String getOrgImage() {
        return orgImage;
    }

    /**
     * @param orgImage
     *        the orgImage to set
     */
    public void setOrgImage(String orgImage) {
        this.orgImage = orgImage;
    }

    /**
     * @return the thumbImage
     */
    public String getThumbImage() {
        return thumbImage;
    }

    /**
     * @param thumbImage
     *        the thumbImage to set
     */
    public void setThumbImage(String thumbImage) {
        this.thumbImage = thumbImage;
    }

    /**
     * @return the user
     */
    public Profile getUser() {
        return user;
    }

    /**
     * @param user
     *        the user to set
     */
    public void setUser(Profile user) {
        this.user = user;
    }

    /**
     * @return the defaultImage
     */
    public Boolean getDefaultImage() {
        return defaultImage;
    }

    /**
     * @param defaultImage
     *        the defaultImage to set
     */
    public void setDefaultImage(Boolean defaultImage) {
        this.defaultImage = defaultImage;
    }

}
