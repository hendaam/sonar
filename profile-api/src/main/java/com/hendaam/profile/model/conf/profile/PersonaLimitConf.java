package com.hendaam.profile.model.conf.profile;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author Simon Ghobreil.
 */
@Component
@PropertySource("classpath:persona.limit.properties")
public class PersonaLimitConf {

    @Value("${profile.limit}")
    private Boolean personaLimitEnable;

    @Value("${profile.limit.num}")
    private Integer personaLimitMax;

    /**
     * @return the personaLimitEnable
     */
    public Boolean getPersonaLimitEnable() {
        return personaLimitEnable;
    }

    /**
     * @param personaLimitEnable
     *        the personaLimitEnable to set
     */
    public void setPersonaLimitEnable(Boolean personaLimitEnable) {
        this.personaLimitEnable = personaLimitEnable;
    }

    /**
     * @return the personaLimitMax
     */
    public Integer getPersonaLimitMax() {
        return personaLimitMax;
    }

    /**
     * @param personaLimitMax
     *        the personaLimitMax to set
     */
    public void setPersonaLimitMax(Integer personaLimitMax) {
        this.personaLimitMax = personaLimitMax;
    }

}
