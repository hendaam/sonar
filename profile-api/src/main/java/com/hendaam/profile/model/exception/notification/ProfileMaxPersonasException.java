package com.hendaam.profile.model.exception.notification;

/**
 * @author Simon Ghobreil.
 */
public class ProfileMaxPersonasException extends ProfileException {

    private static final long serialVersionUID = 1L;

    public ProfileMaxPersonasException() {
        super("205");
    }

}
