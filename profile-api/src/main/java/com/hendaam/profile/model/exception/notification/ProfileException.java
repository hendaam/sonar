package com.hendaam.profile.model.exception.notification;

/**
 * @author Simon Ghobreil.
 */
public class ProfileException extends Exception {

    private static final long serialVersionUID = 1L;

    public ProfileException() {
    }

    public ProfileException(String statusCode) {
        super();
        this.statusCode = statusCode;
    }

    private String statusCode;

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode
     *        the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
