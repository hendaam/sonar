package com.hendaam.profile.model.vo.profile;


/**
 * @author sghobrei
 */
public class ProfileImageResponse extends ProfileResponse {

    private String orginalImagePath;
    private String thumbImagePath;


    /**
     * @return the orginalImagePath
     */
    public String getOrginalImagePath() {
        return orginalImagePath;
    }

    /**
     * @param orginalImagePath
     *        the orginalImagePath to set
     */
    public void setOrginalImagePath(String orginalImagePath) {
        this.orginalImagePath = orginalImagePath;
    }

    /**
     * @return the thumbImagePath
     */
    public String getThumbImagePath() {
        return thumbImagePath;
    }

    /**
     * @param thumbImagePath
     *        the thumbImagePath to set
     */
    public void setThumbImagePath(String thumbImagePath) {
        this.thumbImagePath = thumbImagePath;
    }

}
