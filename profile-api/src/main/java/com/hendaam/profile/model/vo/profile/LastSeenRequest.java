package com.hendaam.profile.model.vo.profile;

import java.util.Date;

/**
 * @author Simon Ghobreil.
 */
public class LastSeenRequest {

    private Date date;

    private String email;

    public LastSeenRequest() {
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date
     *        the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *        the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

}
