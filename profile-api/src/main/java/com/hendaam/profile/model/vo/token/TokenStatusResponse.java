package com.hendaam.profile.model.vo.token;

/**
 * @author Simon Ghobreil.
 */
public class TokenStatusResponse {

    private String statusCode;

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode
     *        the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
