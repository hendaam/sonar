package com.hendaam.profile.model.vo.persona;

import java.util.List;


/**
 * @author sghobrei
 */
public class PersonaRequestWrapper {
    private List<PersonaResponse> personas;

    public List<PersonaResponse> getPersonas() {
        return personas;
    }

    public void setPersonas(List<PersonaResponse> personas) {
        this.personas = personas;
    }
}
