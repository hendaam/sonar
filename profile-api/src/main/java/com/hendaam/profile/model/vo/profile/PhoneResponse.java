package com.hendaam.profile.model.vo.profile;

import com.hendaam.profile.model.vo.Response;

/**
 * This is profile response
 *
 * @author Simon Ghobreil
 */
public class PhoneResponse extends Response {

    private String phone;

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     *        the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

}
