package com.hendaam.profile.model.vo.notification;

/**
 * This is profile response
 *
 * @author Simon Ghobreil
 */
public class EmailResponse {

    private String token;

    private String statusCode;

    public EmailResponse() {
    }

    public EmailResponse(String token, String statusCode) {
        super();
        this.token = token;
        this.statusCode = statusCode;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *        the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode
     *        the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
