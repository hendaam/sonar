package com.hendaam.profile.model.vo.profile;

import java.util.List;

import com.hendaam.profile.model.persona.StylistPreferences;
import com.hendaam.profile.model.profile.Profile;

/**
 * This object will be used for request.
 *
 * @author Simon Ghobreil.
 */
public class ProfileRequest {

    private Profile profile;

    private List<StylistPreferences> stylistPreferences;

    /**
     * @return the profile
     */
    public Profile getProfile() {
        return profile;
    }

    /**
     * @param profile
     *        the profile to set
     */
    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    /**
     * @return the stylistPreferences
     */
    public List<StylistPreferences> getStylistPreferences() {
        return stylistPreferences;
    }

    /**
     * @param stylistPreferences
     *        the stylistPreferences to set
     */
    public void setStylistPreferences(List<StylistPreferences> stylistPreferences) {
        this.stylistPreferences = stylistPreferences;
    }

}
