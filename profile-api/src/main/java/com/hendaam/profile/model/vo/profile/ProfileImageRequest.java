package com.hendaam.profile.model.vo.profile;

/**
 * @author sghobrei
 */
public class ProfileImageRequest {

    private String email;

    private String orginalImagePath;

    private String thumbImagePath;

    /**
     * @param email
     * @param orginalImagePath
     * @param thumbImagePath
     */
    public ProfileImageRequest(String email, String orginalImagePath, String thumbImagePath) {
        super();
        this.email = email;
        this.orginalImagePath = orginalImagePath;
        this.thumbImagePath = thumbImagePath;
    }

    /**
     * @param orginalImagePath
     * @param thumbImagePath
     */
    public ProfileImageRequest(String orginalImagePath, String thumbImagePath) {
        super();
        this.orginalImagePath = orginalImagePath;
        this.thumbImagePath = thumbImagePath;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *        the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the orginalImagePath
     */
    public String getOrginalImagePath() {
        return orginalImagePath;
    }

    /**
     * @param orginalImagePath
     *        the orginalImagePath to set
     */
    public void setOrginalImagePath(String orginalImagePath) {
        this.orginalImagePath = orginalImagePath;
    }

    /**
     * @return the thumbImagePath
     */
    public String getThumbImagePath() {
        return thumbImagePath;
    }

    /**
     * @param thumbImagePath
     *        the thumbImagePath to set
     */
    public void setThumbImagePath(String thumbImagePath) {
        this.thumbImagePath = thumbImagePath;
    }

}
