package com.hendaam.profile.model.vo.persona;

import java.util.List;

import com.hendaam.profile.model.persona.PersonaStatus;
import com.hendaam.profile.model.vo.profile.ProfileResponse;

public class PersonaResponse extends ProfileResponse {
    private List<AbstractStylistPreferences> stylistPreferences;
    private String personaName;
    private Boolean completed;
    private Boolean defaultPersona;
    private PersonaStatus personaStatus;
    private String personaGender;
    private Long id;

    public PersonaResponse() {
    }

    public PersonaResponse(List<AbstractStylistPreferences> stylistPreferences, String personaName, Boolean completed, Boolean defaultPersona, PersonaStatus personaStatus, String personaGender) {
        this.stylistPreferences = stylistPreferences;
        this.personaName = personaName;
        this.completed = completed;
        this.defaultPersona = defaultPersona;
        this.personaStatus = personaStatus;
        this.personaGender = personaGender;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPersonaGender() {
        return personaGender;
    }

    public void setPersonaGender(String personaGender) {
        this.personaGender = personaGender;
    }

    public List<AbstractStylistPreferences> getStylistPreferences() {
        return stylistPreferences;
    }

    public void setStylistPreferences(List<AbstractStylistPreferences> stylistPreferences) {
        this.stylistPreferences = stylistPreferences;
    }

    public String getPersonaName() {
        return personaName;
    }

    public void setPersonaName(String personaName) {
        this.personaName = personaName;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Boolean getDefaultPersona() {
        return defaultPersona;
    }

    public void setDefaultPersona(Boolean defaultPersona) {
        this.defaultPersona = defaultPersona;
    }

    public PersonaStatus getPersonaStatus() {
        return personaStatus;
    }

    public void setPersonaStatus(PersonaStatus personaStatus) {
        this.personaStatus = personaStatus;
    }
}