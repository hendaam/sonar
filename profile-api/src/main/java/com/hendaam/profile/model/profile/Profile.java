package com.hendaam.profile.model.profile;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hendaam.profile.model.persona.Persona;

/**
 * This class represent profile object.
 *
 * @author Simon Ghobreil
 */
@Entity
@Table(name = "profile")
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "profile_id")
    private Long id;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "phone", unique = true, nullable = true)
    private String phone;

    @Column(name = "address")
    private String address;

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "password_hash", nullable = false)
    private String passwordHash;

    @Column(name = "hear_about_us")
    private String howDoHearAboutUS;

    @Column(name = "join_date")
    private Date joinDate;

    @Column(name = "gender")
    private String gender;

    @Column(name = "invited")
    private Boolean invited;

    @Column(name = "enable")
    private Boolean enable;

    @Column(name = "last_seen_login")
    private Date lastLoginTime;

    @Column(name = "last_seen_logout")
    private Date lastLogoutTime;

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToMany(mappedBy = "profile", targetEntity = Persona.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Persona> personas;

    /**
     * @return the enable
     */
    public Boolean getEnable() {
        return enable;
    }

    /**
     * @param enable
     *        the enable to set
     */
    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *        the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName
     *        the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     *        the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address
     *        the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *        the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the passwordHash
     */
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * @param passwordHash
     *        the passwordHash to set
     */
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    /**
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * @param role
     *        the role to set
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * @return the howDoHearAboutUS
     */
    public String getHowDoHearAboutUS() {
        return howDoHearAboutUS;
    }

    /**
     * @param howDoHearAboutUS
     *        the howDoHearAboutUS to set
     */
    public void setHowDoHearAboutUS(String howDoHearAboutUS) {
        this.howDoHearAboutUS = howDoHearAboutUS;
    }

    /**
     * @return the joinDate
     */
    public Date getJoinDate() {
        return joinDate;
    }

    /**
     * @param joinDate
     *        the joinDate to set
     */
    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender
     *        the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the invited
     */
    public Boolean getInvited() {
        return invited;
    }

    /**
     * @param invited
     *        the invited to set
     */
    public void setInvited(Boolean invited) {
        this.invited = invited;
    }

    /**
     * @return the personas
     */
    public List<Persona> getPersonas() {
        return personas;
    }

    /**
     * @param personas
     *        the personas to set
     */
    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }

    /**
     * @return the lastLoginTime
     */
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    /**
     * @param lastLoginTime
     *        the lastLoginTime to set
     */
    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    /**
     * @return the lastLogoutTime
     */
    public Date getLastLogoutTime() {
        return lastLogoutTime;
    }

    /**
     * @param lastLogoutTime
     *        the lastLogoutTime to set
     */
    public void setLastLogoutTime(Date lastLogoutTime) {
        this.lastLogoutTime = lastLogoutTime;
    }

}
