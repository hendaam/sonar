package com.hendaam.profile.model.exception.common;

/**
 * @author Simon Ghobreil.
 */
public class ConnectionRefusedException extends GeneralException {

    private static final long serialVersionUID = 1L;

    public ConnectionRefusedException() {
        super("410");
    }

}
