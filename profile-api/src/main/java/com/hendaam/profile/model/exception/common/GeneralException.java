package com.hendaam.profile.model.exception.common;

/**
 * @author Simon Ghobreil.
 */
public class GeneralException extends Exception {

    private static final long serialVersionUID = 1L;

    public GeneralException() {
    }

    public GeneralException(String statusCode) {
        super();
        this.statusCode = statusCode;
    }

    private String statusCode;

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode
     *        the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
