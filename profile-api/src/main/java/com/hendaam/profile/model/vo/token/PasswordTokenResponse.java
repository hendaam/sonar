package com.hendaam.profile.model.vo.token;

import com.hendaam.profile.model.profile.Profile;

/**
 * @author Simon Ghobreil.
 */
public class PasswordTokenResponse extends TokenResponse {

    private Profile profile;

    /**
     * @return the profile
     */
    public Profile getProfile() {
        return profile;
    }

    /**
     * @param profile
     *        the profile to set
     */
    public void setProfile(Profile profile) {
        this.profile = profile;
    }

}
