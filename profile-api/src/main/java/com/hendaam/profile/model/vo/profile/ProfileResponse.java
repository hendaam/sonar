package com.hendaam.profile.model.vo.profile;

import com.hendaam.profile.model.vo.Response;

/**
 * This is profile response
 *
 * @author Simon Ghobreil
 */
public class ProfileResponse extends Response {

    private String email;

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *        the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

}
