package com.hendaam.profile.model.conf.restclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Rest Client Config.
 *
 * @author Simon Ghobreil.
 */
@Component
@PropertySource("classpath:rest_client_conf.properties")
public class RestClientConfig {

    @Value("${notification.service.base.url}")
    private String notificationServerURL;

    @Value("${notification.service.username}")
    private String notificationUserName;

    @Value("${notification.service.password}")
    private String notificationPassword;

    /**
     * @return the notificationServerURL
     */
    public String getNotificationServerURL() {
        return notificationServerURL;
    }

    /**
     * @param notificationServerURL
     *        the notificationServerURL to set
     */
    public void setNotificationServerURL(String notificationServerURL) {
        this.notificationServerURL = notificationServerURL;
    }

    /**
     * @return the notificationUserName
     */
    public String getNotificationUserName() {
        return notificationUserName;
    }

    /**
     * @param notificationUserName
     *        the notificationUserName to set
     */
    public void setNotificationUserName(String notificationUserName) {
        this.notificationUserName = notificationUserName;
    }

    /**
     * @return the notificationPassword
     */
    public String getNotificationPassword() {
        return notificationPassword;
    }

    /**
     * @param notificationPassword
     *        the notificationPassword to set
     */
    public void setNotificationPassword(String notificationPassword) {
        this.notificationPassword = notificationPassword;
    }

}
