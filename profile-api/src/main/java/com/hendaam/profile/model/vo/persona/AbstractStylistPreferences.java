package com.hendaam.profile.model.vo.persona;

public class AbstractStylistPreferences {
    private String question;

    private String answer;

    public AbstractStylistPreferences() {
    }

    public AbstractStylistPreferences(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}