package com.hendaam.profile.model.persona;

public enum PersonaStatus {

    INCOMPLETE("INCOMPLETE"), INCOMPLETE_SKIP("INCOMPLETE_SKIP"), COMPLETED("COMPLETED");

    private String status;

    private PersonaStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}
