package com;

import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.builders.PathSelectors.regex;

import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = { "com.hendaam.*" })
@EnableJpaRepositories(basePackages = { "com.hendaam.*" })
@Import(CustomizedRestMvcConfiguration.class)
@EnableAutoConfiguration
@EntityScan(basePackages = { "com.hendaam.*" })
@EnableAsync
@EnableSwagger2
public class HendaamProfileApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(HendaamProfileApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(HendaamProfileApplication.class);
    }



    @Bean
    public Docket newsApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("Profile").apiInfo(apiInfo()).select()
                .paths(regex("/api.*")).build().securitySchemes(newArrayList(apiKeyBasicAuth()))
                .securityContexts(newArrayList(securityContext()));
    }

    private BasicAuth apiKeyBasicAuth() {
        return new BasicAuth("BasicAuth");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/.*")).build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("BasicAuth", "basic");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return newArrayList(new SecurityReference("BasicAuth", authorizationScopes));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("Hendaam profile api")
                .description("Profile API manage customers profiles").contact("Simon Ghobreil")
                .license("Hendaam LT.").version("1.0").build();
    }

}
