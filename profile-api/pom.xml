<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.hendaam.profile</groupId>
	<artifactId>profile-api</artifactId>
	<version>1.0.0</version>

	<name>profile-api</name>
	<description>This is hendaam profile service</description>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.3.0.RELEASE</version>
		<relativePath /> <!-- lookup parent from repository -->
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<java.version>1.8</java.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-rest</artifactId>
		</dependency>

		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<scope>runtime</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.hateoas</groupId>
			<artifactId>spring-hateoas</artifactId>
		</dependency>

		<dependency>
			<groupId>com.jayway.jsonpath</groupId>
			<artifactId>json-path</artifactId>
			<scope>test</scope>
		</dependency>



		<dependency>
			<groupId>org.liquibase</groupId>
			<artifactId>liquibase-core</artifactId>
		</dependency>

		<dependency>
			<groupId>org.liquibase</groupId>
			<artifactId>liquibase-maven-plugin</artifactId>
			<version>3.3.5</version>
		</dependency>

		<!-- Swagger -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>2.2.2</version>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>2.2.2</version>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-staticdocs</artifactId>
			<version>2.2.2</version>
			<scope>test</scope>
		</dependency>

		<!-- SDK -->
		<dependency>
			<groupId>com.hendaam.notification</groupId>
			<artifactId>notification-sdk</artifactId>
			<version>1.0.0</version>
		</dependency>

	</dependencies>



	<distributionManagement>
		<repository>
			<id>nexus</id>
			<url>http://hendaamnexus.westeurope.cloudapp.azure.com:8081/repository/hendaam</url>
		</repository>
	</distributionManagement>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>

			<plugin>
				<groupId>org.liquibase</groupId>
				<artifactId>liquibase-maven-plugin</artifactId>
				<version>3.4.1</version>
				<configuration>
					<changeLogFile>src/main/resources/db/changelog/liquibase-outputChangeLog.xml</changeLogFile>
					<propertyFile>src/main/resources/liquibase.properties</propertyFile>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<profiles>
		<profile>
			<id>GenerateDiff</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.liquibase</groupId>
						<artifactId>liquibase-maven-plugin</artifactId>
						<version>3.4.1</version>
						<configuration>
							<propertyFile>src/main/resources/liquibase.properties</propertyFile>
							<changeLogFile>src/main/resources/db/changelog/liquibase-outputChangeLog.xml</changeLogFile>
							<diffChangeLogFile>src/main/resources/db/changelog/db-${timestamp}.changelog.xml</diffChangeLogFile>
							<verbose>true</verbose>
							<logging>debug</logging>
						</configuration>
						<executions>
							<execution>
								<phase>generate-sources</phase>
								<goals>
									<goal>diff</goal>
								</goals>
							</execution>
						</executions>
						<dependencies>
							<dependency>
								<groupId>org.liquibase.ext</groupId>
								<artifactId>liquibase-hibernate4</artifactId>
								<version>3.5</version>
							</dependency>
							<dependency>
								<groupId>org.springframework</groupId>
								<artifactId>spring-beans</artifactId>
								<version>4.1.7.RELEASE</version>
							</dependency>
							<dependency>
								<groupId>org.springframework.data</groupId>
								<artifactId>spring-data-jpa</artifactId>
								<version>1.7.3.RELEASE</version>
							</dependency>
						</dependencies>
					</plugin>
				</plugins>
			</build>
		</profile>



		<profile>
			<id>local</id>
			<build>
				<plugins>
					<plugin>
						<artifactId>maven-antrun-plugin</artifactId>
						<version>1.7</version>
						<executions>
							<execution>
								<phase>compile</phase>
								<goals>
									<goal>run</goal>
								</goals>
								<configuration>
									<target>
										<echo message="Reading properties from dev " level="info" />

										<delete file="src/main/resources/application.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/local/application.dev.properties"
											tofile="src/main/resources/application.properties" />

										<delete file="src/main/resources/liquibase.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/local/liquibase.dev.properties"
											tofile="src/main/resources/liquibase.properties" />

										<delete file="src/main/resources/rest_client_conf.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/local/rest.dev.properties"
											tofile="src/main/resources/rest_client_conf.properties" />

										<delete file="src/main/resources/profile.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/local/profile.dev.properties"
											tofile="src/main/resources/profile.properties" />
									</target>
								</configuration>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.liquibase</groupId>
						<artifactId>liquibase-maven-plugin</artifactId>
						<version>3.4.1</version>
						<configuration>
							<propertyFile>src/main/resources/liquibase.properties</propertyFile>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>


		<profile>
			<id>dev-env</id>
			<build>
				<plugins>
					<plugin>
						<artifactId>maven-antrun-plugin</artifactId>
						<version>1.7</version>
						<executions>
							<execution>
								<phase>compile</phase>
								<goals>
									<goal>run</goal>
								</goals>
								<configuration>
									<target>
										<echo message="Reading properties from dev " level="info" />

										<delete file="src/main/resources/application.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/dev/application.dev.env.properties"
											tofile="src/main/resources/application.properties" />

										<delete file="src/main/resources/liquibase.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/dev/liquibase.dev.env.properties"
											tofile="src/main/resources/liquibase.properties" />

										<delete file="src/main/resources/rest_client_conf.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/dev/rest.dev.env.properties"
											tofile="src/main/resources/rest_client_conf.properties" />

										<delete file="src/main/resources/profile.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/dev/profile.dev.env.properties"
											tofile="src/main/resources/profile.properties" />
									</target>
								</configuration>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.liquibase</groupId>
						<artifactId>liquibase-maven-plugin</artifactId>
						<version>3.4.1</version>
						<configuration>
							<propertyFile>src/main/resources/liquibase.properties</propertyFile>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>

		<profile>
			<id>testing</id>
			<build>
				<plugins>
					<plugin>
						<artifactId>maven-antrun-plugin</artifactId>
						<version>1.7</version>
						<executions>
							<execution>
								<phase>compile</phase>
								<goals>
									<goal>run</goal>
								</goals>

								<configuration>
									<target>
										<echo message="Reading properties from dev " level="info" />

										<delete file="src/main/resources/application.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/test/application.testing.properties"
											tofile="src/main/resources/application.properties" />

										<delete file="src/main/resources/liquibase.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/test/liquibase.testing.properties"
											tofile="src/main/resources/liquibase.properties" />

										<delete file="src/main/resources/rest_client_conf.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/test/rest.testing.properties"
											tofile="src/main/resources/rest_client_conf.properties" />

										<delete file="src/main/resources/profile.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/test/profile.testing.properties"
											tofile="src/main/resources/profile.properties" />
									</target>
								</configuration>
							</execution>
						</executions>
					</plugin>


					<plugin>
						<groupId>org.liquibase</groupId>
						<artifactId>liquibase-maven-plugin</artifactId>
						<version>3.4.1</version>
						<configuration>
							<propertyFile>src/main/resources/liquibase.properties</propertyFile>
						</configuration>
					</plugin>

				</plugins>
			</build>
		</profile>

		<profile>
			<id>production</id>
			<build>
				<plugins>
					<plugin>
						<artifactId>maven-antrun-plugin</artifactId>
						<version>1.7</version>
						<executions>
							<execution>
								<phase>install</phase>
								<goals>
									<goal>run</goal>
								</goals>

								<configuration>
									<target>
										<echo message="Reading properties from dev " level="info" />

										<delete file="src/main/resources/application.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/prod/application.prod.properties"
											tofile="src/main/resources/application.properties" />

										<delete file="src/main/resources/liquibase.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/prod/liquibase.prod.properties"
											tofile="src/main/resources/liquibase.properties" />

										<delete file="src/main/resources/rest_client_conf.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/prod/rest.prod.properties"
											tofile="src/main/resources/rest_client_conf.properties" />

										<delete file="src/main/resources/profile.properties" />
										<copy
											file="../hendaam-profile/src/main/resources/conf/prod/profile.prod.properties"
											tofile="src/main/resources/profile.properties" />
									</target>
								</configuration>
							</execution>
						</executions>
					</plugin>

					<plugin>
						<groupId>org.liquibase</groupId>
						<artifactId>liquibase-maven-plugin</artifactId>
						<version>3.4.1</version>
						<configuration>
							<propertyFile>src/main/resources/liquibase.properties</propertyFile>
						</configuration>
					</plugin>

				</plugins>
			</build>
		</profile>
	</profiles>


</project>
